package com.dieselProjects.intradayReciver;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.LinkedList;

import com.dieselProjects.beans.DataBase;
import com.dieselProjects.beans.Tick;

public class IntradayReciever {

	public synchronized LinkedList<Tick> getStockIND1M(String ticker) {

		LinkedList<Tick> ticks = new LinkedList<Tick>();
		String reqURL = "http://chartapi.finance.yahoo.com/instrument/1.0/"
				+ ticker + "/chartdata;type=quote;range=1d/csv";
		URL yahoofin;
		try {
			yahoofin = new URL(reqURL);
			URLConnection yc = yahoofin.openConnection();
			BufferedReader in = new BufferedReader(new InputStreamReader(
					yc.getInputStream()));

			String inputLine;
			while ((inputLine = in.readLine()) != null) {

				String[] yahooStockLine = inputLine.split(":");
				if (yahooStockLine[0].equals("volume")) {
					break;
				}
			}
			while ((inputLine = in.readLine()) != null) {

				String[] yahooStockLine = inputLine.split(",");

				Calendar date = new GregorianCalendar();
				date.setTimeInMillis(Long.valueOf(yahooStockLine[0]) * 1000);

				// disregard last tick
				Tick stockTick = new Tick(ticker,
						Double.valueOf(yahooStockLine[4]),// open
						Double.valueOf(yahooStockLine[2]),// high
						Double.valueOf(yahooStockLine[3]),// low
						Double.valueOf(yahooStockLine[1]),// close
						Integer.valueOf(yahooStockLine[5]),// volume
						date);// date in Calendar format
				ticks.add(stockTick);
			}
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return ticks;
	}

	// This is synched so we only do one request at a time
	// If yahoo doesn't return stock info we will try to return it from the map
	// in memory
	public synchronized LinkedList<LinkedList<Tick>> getStockIND5M(String ticker,
			int numOfDays) {

		LinkedList<LinkedList<Tick>> ticksArray = new LinkedList<LinkedList<Tick>>();
		LinkedList<Tick> ticks = new LinkedList<Tick>();
		
		if (numOfDays == 1) {
			try {
				String reqURL = "http://chartapi.finance.yahoo.com/instrument/1.0/"
						+ ticker + "/chartdata;type=quote;range=2d/csv";

				URL yahoofin = new URL(reqURL);
				URLConnection yc = yahoofin.openConnection();
				BufferedReader in = new BufferedReader(new InputStreamReader(
						yc.getInputStream()));

				String inputLine;
				while ((inputLine = in.readLine()) != null) {

					String[] yahooStockLine = inputLine.split(":");
					if (yahooStockLine[0].equals("volume")) {
						break;
					}
				}
				int counter = 1;
				while ((inputLine = in.readLine()) != null) {

					String[] yahooStockLine = inputLine.split(",");

					Calendar date = new GregorianCalendar();
					date.setTimeInMillis(Long.valueOf(yahooStockLine[0]) * 1000);

					// disregard last tick
					if (counter < 79) {
						Tick stockTick = new Tick(ticker,
								Double.valueOf(yahooStockLine[4]),// open
								Double.valueOf(yahooStockLine[2]),// high
								Double.valueOf(yahooStockLine[3]),// low
								Double.valueOf(yahooStockLine[1]),// close
								Integer.valueOf(yahooStockLine[5]),// volume
								date);// date in Calendar format
						ticks.add(stockTick);
						counter++;
					} else {
						ticksArray.add(ticks);
						break;
					}
				}
				in.close();
			} catch (Exception ex) {
				System.err.println("Unable to get stockinfo for: " + ticker
						+ ex);
			}
		} else {
			try {

				// yahoo intraday api:
				//
				// request
				// "http://chartapi.finance.yahoo.com/instrument/1.0/{STOCK}/chartdata;type=quote;range={backDayFromNow}/csv"
				// response:
				// range - each days date (actual date, start time stamp, end
				// time
				// stamp)
				// after volume
				// timestamp, close, high, low, open, volume

				String reqURL = "http://chartapi.finance.yahoo.com/instrument/1.0/"
						+ ticker
						+ "/chartdata;type=quote;range="
						+ numOfDays
						+ "d/csv";

				URL yahoofin = new URL(reqURL);
				URLConnection yc = yahoofin.openConnection();
				BufferedReader in = new BufferedReader(new InputStreamReader(
						yc.getInputStream()));

				String inputLine;
				while ((inputLine = in.readLine()) != null) {

					String[] yahooStockLine = inputLine.split(":");
					if (yahooStockLine[0].equals("volume")) {
						break;
					}
				}
				int counter = 1;
				while ((inputLine = in.readLine()) != null) {

					String[] yahooStockLine = inputLine.split(",");

					Calendar date = new GregorianCalendar();
					date.setTimeInMillis(Long.valueOf(yahooStockLine[0]) * 1000);

					// disregard last tick
					if ((counter % 79) != 0) {
						Tick stockTick = new Tick(ticker,
								Double.valueOf(yahooStockLine[4]),// open
								Double.valueOf(yahooStockLine[2]),// high
								Double.valueOf(yahooStockLine[3]),// low
								Double.valueOf(yahooStockLine[1]),// close
								Integer.valueOf(yahooStockLine[5]),// volume
								date);// date in Calendar format
						ticks.add(stockTick);
					} else {
						ticksArray.add(ticks);
						ticks = new LinkedList<Tick>();
					}
					counter++;
				}
				in.close();
			} catch (Exception ex) {
				System.err.println("Unable to get stockinfo for: " + ticker
						+ ex);
			}
		}

		return ticksArray;
	}

	public DataBase loadDB(File fileDB) {
		DataBase newDB = null;

		try {
			InputStream is = new FileInputStream(fileDB);
			InputStream bf = new BufferedInputStream(is);
			ObjectInput oInput = new ObjectInputStream(bf);
			newDB = (DataBase) oInput.readObject();
			oInput.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return newDB;

	}
}