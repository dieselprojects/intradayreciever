package com.dieselProjects.intradayReciver;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;

import com.dieselProjects.beans.OneStockDB;
import com.dieselProjects.beans.Tick;
import com.dieselProjects.beans.TimeInst;
import com.dieselProjects.support.advTimeMan;

public class IntradayThread implements Runnable {

	advTimeMan timeMan;
	IntradayReciever intradayReciever = new IntradayReciever();
	public String type;
	public String[] stockArray;
	public int numberOfBackDays;
	public boolean simMode = false;
	SimpleDateFormat sdf = new SimpleDateFormat("dd_MM_yyyy");

	public IntradayThread(String type, String[] stockArray,
			int numberOfBackDays, boolean simMode) {
		timeMan = new advTimeMan();
		intradayReciever = new IntradayReciever();
		this.stockArray = stockArray;
		this.type = type;
		this.numberOfBackDays = numberOfBackDays;
		this.simMode = simMode;
	}

	public void run() {

		if (simMode) {
			iteration();
		} else {

			while (true) {
				TimeInst timeInterval = timeMan.getNextTickTime();
				try {
					Thread.sleep(timeInterval.timeInMs);
					iteration();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	public void iteration() {

		for (String ticker : stockArray) {

			if (type.equals("5M")) {

				// get all supported stocks to store
				LinkedList<LinkedList<Tick>> ticksArray = intradayReciever
						.getStockIND5M(ticker, numberOfBackDays);

				Iterator<LinkedList<Tick>> itr = ticksArray.iterator();

				while (itr.hasNext()) {
					LinkedList<Tick> nextTickList = itr.next();
					Calendar dateO = nextTickList.getFirst().date;

					OneStockDB nextStockDB = new OneStockDB(ticker,
							sdf.format(dateO.getTime()), type, nextTickList);
					nextStockDB.store();

				}

			} else {

				LinkedList<Tick> ticks;
				ticks = intradayReciever.getStockIND1M(ticker);

				Calendar dateO = ticks.getFirst().date;

				OneStockDB nextStockDB = new OneStockDB(ticker,
						sdf.format(dateO.getTime()), type, ticks);
				nextStockDB.store();

			}
		}

	}

}
