package com.dieselProjects.intradayReciver;

public class IntradayRecieverMain {

	static final int DATA_BASE_TYPE_OFFSET = 0;
	static final int NUMBER_OF_BACK_DAYS_OFFSET = 1;
	static final int SIM_MODE_OFFSET = 2;
	static final int NUMBER_OF_REQ_ARGS = 2;

	// How data base is saved:
	// two directories in a certain location:
	// 5M - 5 minute directory - file for each day mark dd_MM_yyyy.db
	// 1M - 1 minute directory - file for each day marked the same

	public static void main(String[] args) {

		String databaseType = "1M";
		int numberOfBackDays = 1;
		boolean simMode = true;
		
		if (args.length != NUMBER_OF_REQ_ARGS) {
			System.err
					.println("Optional input argument, use [DATABASE TYPE, DAYS, USE DB]");
			System.out.println("Using 1M 1 day and false for using previous data base");
		
		} else {
			databaseType = args[DATA_BASE_TYPE_OFFSET];
			numberOfBackDays = Integer.parseInt(args[NUMBER_OF_BACK_DAYS_OFFSET]);
			simMode = Boolean.parseBoolean(args[SIM_MODE_OFFSET]);
		}
		
		String[] stockArray = { "AAPL", "IBM", "FB", "GOOG", "FSL", "INTC",
				"AMZN", "DIS", "ARMH", "YHOO", "MSFT", "NFLX", "CSCO", "PTNR" };

		IntradayThread intradayThread = new IntradayThread(databaseType,stockArray,numberOfBackDays,simMode);
		intradayThread.run();

	}

}
