package com.dieselProjects.beans;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;

public class DataBase implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	String dataBaseDescription = "";
	LinkedList<String> stocksNames;
	HashMap<String, LinkedList<Tick>> database = new HashMap<String, LinkedList<Tick>>();

	public DataBase(String dataBaseDescription) {
		this.dataBaseDescription = dataBaseDescription;
		stocksNames = new LinkedList<String>();
		database = new HashMap<String, LinkedList<Tick>>();
	}

	public void add(String ticker, LinkedList<Tick> list) {

		// check if ticker exists
		if (stocksNames.contains(ticker)) {
			database.get(ticker).addAll(list);
		} else {
			stocksNames.add(ticker);
			database.put(ticker, list);
		}
	}

	public void addDB(DataBase additionalDB) {

		for (String nextStock : additionalDB.stocksNames) {

			if (this.stocksNames.contains(nextStock)){
				database.get(nextStock).addAll(additionalDB.database.get(nextStock));
			} else{
				stocksNames.add(nextStock);
				database.put(nextStock, additionalDB.database.get(nextStock));
			}
				
		}

	}

	
	
	
	
	
	// Load all files inside given directory and store them in a common
	// Database:
	public void loadDB(String directoryName) {
		// use directory to load all files into a single data base

		File directory = new File(directoryName);
		File[] fList = directory.listFiles(new FilenameFilter() {
			public boolean accept(File arg0, String arg1) {
				return arg1.toLowerCase().endsWith(".db");
			}
		});

		// LastModifiedFileComparator.LASTMODIFIED_COMPARATOR.sort(fList);
		for (File file : fList) {
			try {
				InputStream nextFile = new FileInputStream(file);
				InputStream buffer = new BufferedInputStream(nextFile);
				ObjectInput oInput = new ObjectInputStream(buffer);
				DataBase nextDataBase = (DataBase) oInput.readObject();
				this.addDB(nextDataBase);
				oInput.close();
			} catch (IOException ex) {
				System.err.println("Error reading Data base files "
						+ ex.getMessage());
				System.exit(2);
			} catch (ClassNotFoundException e) {
				System.err.println("Error reading Data base files "
						+ e.getMessage());
				System.exit(2);
			}
		}
	}
}
