package com.dieselProjects.beans;

import java.io.Serializable;
import java.util.Calendar;

public class Tick implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public String 	ticker;
	public Double 	openPrice;
	public Double 	highPrice;
	public Double 	lowPrice;
	public Double 	closePrice;
	public Integer 	volume;
	public Calendar	date;
	
	public Tick(String 	ticker,
			Double 		openPrice,
			Double 		highPrice,
			Double 		lowPrice,
			Double 		closePrice,
			Integer		volume,
			Calendar 	date
			
	){
		this.ticker		= ticker	;
		this.openPrice	= openPrice	;
		this.closePrice	= closePrice;
		this.highPrice	= highPrice	;
		this.lowPrice	= lowPrice	;
		this.volume		= volume	;
		this.date		= date		;
	}
	
}
