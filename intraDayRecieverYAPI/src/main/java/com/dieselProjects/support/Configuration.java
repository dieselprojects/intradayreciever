package com.dieselProjects.support;

public class Configuration {

	// Singleton Implementation:
	// -------------------------

	private static final Configuration GeneralCFG = new Configuration();

	public static Configuration getInstance() {
		return GeneralCFG;
	}

	public static final int NUM_TA_INDICATORS = 3;
	public static final int NUM_CDL_INDICATORS = 61;
	public static final int TOTAL_NUM_INDICATORS = NUM_TA_INDICATORS + NUM_CDL_INDICATORS;
	
	// ----------------------------------- //
	// - General Configuration Variables - //
	// ----------------------------------- //
	public String twsHost = "127.0.0.1";
	public int twsPort = 4001;
	public int twsID = 0;


	// FileIo Configuration:
	// ---------------------
	// allStocks - list of all available stocks
	// filteredStockFile - list of filtered stocks to trade with
	// storeDBFile - location of DB dump
	// logFile - location of log file
	public String allStocks = "Res/stockList.csv";
	public String filteredStockFile = "/filteredStocks.csv";
	public String storeDBFile = "Res/stockDB.DBF";
	public String logFile = "Res/stockLog.log";

	private Configuration() {
	}

	public void print() {
		System.out.println("-----------------------------------------");
		System.out.println("--   INTRADAY RECIEVER CONFIGURATION   --");
		System.out.println("-----------------------------------------\n\n");
		System.out.println("General configuration:");
		System.out.println("----------------------------");
		System.out.println("--   END CONFIGURATION    --");
		System.out.println("----------------------------\n\n");
	}

}
