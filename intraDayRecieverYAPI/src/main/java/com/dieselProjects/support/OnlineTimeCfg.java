package com.dieselProjects.support;

public class OnlineTimeCfg {
	
	private static final OnlineTimeCfg onlineTimeCfg = new OnlineTimeCfg();
	
    // fullSimulatedOnlineMode - Simulated acual trade cycles, need online broker and internet connection
	public static Boolean fullSimulatedOnlineMode = true;
	
	// Trade Time:
	// -----------
    // tradeOpen  	   -  market open time
    // tradeClose 	   -  market close time
    // algoTradeStart  -  start trade time
    // halfDayClose	   -  half day market close
    // algoItration	   -  itiration between each trade cycle
    // dailyTickTime   -  program store daily tick at this time
	public static String tradeOpen      = "16:30";
	public static String tradeClose     = "23:00";
	public static String algoTradeStart = "17:00";
	public static String halfDayClose   = "20:00";
	public static String algoItration   = "0:01";
	public static String dailyTickTime  = "23:30";
	
	
	
	// NewYorkTradeDays:
	// -----------------
        //TradeDays  - week trade days
        //TradeClose - holiday market close days
        //HalfDay    - holiday half day trade
	public static String    TradeDays   = "2,3,4,5,6"; // monday-friday
	public static String[]  TradeClose  = {"01.01","20.01","17.02","18.04","26.5","04.07","01.09","27.11","27.12"};
	public static String[]  HalfDay     = {"03.07","28.11","24.12"}; 
	
	
	public static OnlineTimeCfg getInstance(){
		return onlineTimeCfg;
	}

        public void print(){
            System.out.println("Time managment configuration:");
            System.out.println("\tfullSimulatedOnlineMode: " + fullSimulatedOnlineMode);
            System.out.println("\ttradeOpen      : " + tradeOpen  	);
            System.out.println("\ttradeClose     : " + tradeClose 	);
            System.out.println("\talgoTradeStart : " + algoTradeStart  );
            System.out.println("\thalfDayClose   : " + halfDayClose	);
            System.out.println("\talgoItration   : " + algoItration	);
            System.out.println("\tdailyTickTime  : " + dailyTickTime	);
            System.out.println("\tTradeDays      : " + TradeDays );
            System.out.println("\tTrade Close days:");
            for (String closeDay : TradeClose){
                System.out.println("\t\t" + closeDay);
            }
            System.out.println("\tHalf day trade:");
            for (String halfDay : HalfDay){
                System.out.println("\t\t" + halfDay);
            }
            System.out.println("-------------------------------");
        }
	
}
