package com.dieselProjects.support;

import java.util.LinkedList;

public class IDRLogger {

	
	private static final IDRLogger idrLogger = new IDRLogger();

	public static IDRLogger getInstance() {
		return idrLogger;
	}
	
	public LinkedList<String> logList = new LinkedList<String>();
	
	
	public void log(String event){
		logList.add(event);
	}
	
}
